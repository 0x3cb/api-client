package constants;

import static dto.enums.GatewayIntent.*;

public class Constants {
    public static final String TOKEN = "NzgwMzYyODMyMzgyMzk0Mzg4.X7t_hQ.f8Tr50Xb4r1GEGirQNW4hylK5CM";
    public static final String OS = "Linux";
    public static final String BROWSER = "Browser";
    public static final String DEVICE = "Device";
    public static final String BASE_URL = "https://discord.com/api";
    public static final String BOT_TOKEN = "Bot " + TOKEN;

    public static final Integer INTENTS = calculateIntents(GUILDS, GUILD_MESSAGES, GUILD_MESSAGE_TYPING, DIRECT_MESSAGES, DIRECT_MESSAGE_TYPING, GUILD_MESSAGE_REACTIONS );
}
