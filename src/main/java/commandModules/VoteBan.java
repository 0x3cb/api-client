package commandModules;

import dto.messages.BaseMessage;
import dto.user.User;

public class VoteBan extends Vote {

    private final User userToBan;

    public VoteBan(String channelId, User user, long expires) {
        super(
                channelId,
                String.format("Ban @%s ?", user.getUsername()),
                expires
        );
        userToBan = user;
    }

    @Override
    void successPostVoteAction() {
        channelEndpoint.createMessage(channelId, new BaseMessage(String.format("Farewell %s!", userToBan.getUsername())));
    }

    @Override
    void failurePostVoteAction() {
        channelEndpoint.createMessage(channelId, new BaseMessage(String.format("This is your lucky day %s!", userToBan.getUsername())));
    }
}
