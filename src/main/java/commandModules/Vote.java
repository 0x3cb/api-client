package commandModules;

import dto.messages.BaseMessage;
import dto.messages.MessageDto;
import dto.messages.Reaction;
import http.endpoints.ChannelEndpoint;

import java.util.Timer;
import java.util.TimerTask;

abstract class Vote extends Timer {
    protected static ChannelEndpoint channelEndpoint = ChannelEndpoint.getInstance();

    protected String channelId;
    protected String messageId;
    protected String voteMessage;
    long expires = 60000;

    abstract void successPostVoteAction();
    abstract void failurePostVoteAction();

    public void startVote(){
        initVoteMessage();
        schedule(new TimerTask() {
            @Override
            public void run() {
                if (countVotes()) successPostVoteAction();
                else failurePostVoteAction();
            }
        }, this.expires);
    }

    private void initVoteMessage(){
        MessageDto newMessage = new BaseMessage(voteMessage);
        MessageDto sentMessage = channelEndpoint.createMessage(this.channelId, newMessage);
        this.messageId = sentMessage.getId();
        String proEmoji = "\uD83D\uDC4D"; String conEmoji = "\uD83D\uDC4E";
        channelEndpoint.createReaction(channelId, messageId, proEmoji);
        channelEndpoint.createReaction(channelId, messageId, conEmoji);
    }

    private boolean countVotes(){
        int pro, con = pro = 0;
        String proEmoji = "\uD83D\uDC4D";
        String conEmoji = "\uD83D\uDC4E";
        MessageDto message = channelEndpoint.getMessage(channelId, messageId);
        for (Reaction reaction : message.getReactions()) {
            if (reaction.getEmoji().getName().equals(proEmoji)) pro = reaction.getCount() - 1;
            else if(reaction.getEmoji().getName().equals(conEmoji)) con = reaction.getCount() - 1;
        }
        return pro > con;
    }

    public Vote( String channelId, String content, long expires) {
        super(false);
        this.channelId = channelId;
        this.voteMessage = content;
        this.expires = expires;
    }
}
