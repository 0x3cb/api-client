import com.neovisionaries.ws.client.WebSocketException;
import factories.HeartbeatSenderFactory;

public class Main {

    public static void main(String[] args) throws WebSocketException {
        factories.WebSocketFactory.getInstance().getWebSocket().connect();
        HeartbeatSenderFactory.getInstance().getHeartbeatSender().start();
        Runtime.getRuntime().addShutdownHook(new Thread(){
            @Override
            public void run() {
                HeartbeatSenderFactory.getInstance().getHeartbeatSender().stop();
                factories.WebSocketFactory.getInstance().getWebSocket().disconnect(1000);
            }
        });
    }
}