package senders;

import com.neovisionaries.ws.client.PayloadGenerator;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketCloseCode;
import com.neovisionaries.ws.client.WebSocketFrame;

import java.util.Timer;
import java.util.TimerTask;

import static state.SessionState.session;

public class HeartbeatSender {

    private final WebSocket webSocket;

    private final PayloadGenerator generator;

    private final Timer timer;

    private long interval;

    private boolean scheduled;

    public HeartbeatSender(WebSocket webSocket, PayloadGenerator generator) {
        this.webSocket = webSocket;
        this.generator = generator;
        this.timer = new Timer("HeartbeatTimer");
    }

    public long getInterval() {
        return interval;
    }

    public synchronized void setInterval(long interval) {
        this.interval = interval < 0 ? 0 : interval;
        if (interval > 0 && !scheduled) {
            scheduled = schedule(timer, new Task(), interval);
        }
    }

    public void start() {
        setInterval(getInterval());
    }

    public synchronized void stop() {
        scheduled = false;
        timer.cancel();
    }

    private boolean schedule(Timer timer, Task task, long interval) {
        try {
            timer.schedule(task, interval);
            return true;
        } catch (RuntimeException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void doTask() {
        synchronized (this) {
            if (interval == 0 || !webSocket.isOpen()) {
                scheduled = false;
                return;
            }

            if (!session().isAckBetweenHeartBeats()) {
                System.err.println("> There was no ACK. Disconnecting... ");
                webSocket.disconnect(WebSocketCloseCode.NONE);
                return;
            }

            webSocket.sendFrame(createFrame());
            session().setAckBetweenHeartBeats(false);
            scheduled = schedule(timer, new Task(), interval);
        }
    }

    private WebSocketFrame createFrame() { return createFrame(generatePayload()); }

    private byte[] generatePayload() {
        if (generator == null) {
            return null;
        }

        try {
            return generator.generate();
        } catch (Throwable t) {
            return null;
        }
    }

    private WebSocketFrame createFrame(byte[] payload) {
        return WebSocketFrame.createBinaryFrame(payload);
    }

    private final class Task extends TimerTask {
        @Override
        public void run() {
            doTask();
        }
    }
}