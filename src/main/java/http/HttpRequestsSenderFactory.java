package http;

import dto.JsonErrorDto;
import http.exceptions.HttpErrorStatusCodeException;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;
import java.util.stream.Stream;

import static constants.Constants.BOT_TOKEN;
import static http.HttpConstants.*;
import static utils.JsonUtils.fromJson;
import static utils.JsonUtils.toJson;

public class HttpRequestsSenderFactory {

    RateLimitModule rateLimitModule = new RateLimitModule();

    private static final Map<String, String> DEFAULT_HEADERS = Map.of(
            AUTHORIZATION, BOT_TOKEN,
            CONTENT_TYPE, APPLICATION_JSON
    );

    private static final HttpClient client = HttpClient.newHttpClient();
    private static HttpRequestsSenderFactory instance;

    public static HttpRequestsSenderFactory getInstance(){
        if(instance == null)
            instance = new HttpRequestsSenderFactory();
        return instance;
    }

    public HttpResponse<String> doGet(String uri) { return doGet(uri, DEFAULT_HEADERS); }

    public HttpResponse<String> doGet(String uri, Map<String, String> headers) {
        HttpResponse<String> response = null;
        try{
            HttpRequest request = buildGet(uri, headers);
            response = send(request);
        } catch (URISyntaxException | InterruptedException | IOException e) {
            e.printStackTrace();
        }
        return response;
    }


    public HttpResponse<String> doPut(String uri, Object data) { return doPut(uri, DEFAULT_HEADERS, data); }

    public HttpResponse<String> doPut(String uri, Map<String, String> headers, Object data) {
        HttpResponse<String> response = null;
        try {
            HttpRequest request = buildPut(uri, headers, data);
            response = send(request);
        } catch (URISyntaxException | IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return response;
    }

    public HttpResponse<String> doPost(String uri, Object data) { return doPost(uri, DEFAULT_HEADERS, data); }

    public HttpResponse<String> doPost(String uri, Map<String, String> headers, Object data) {
        HttpResponse<String> response = null;
        try {
            HttpRequest request = buildPost(uri, headers, data);
            response = send(request);
        } catch (URISyntaxException | IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return response;
    }

    private HttpResponse<String> send(HttpRequest request) throws IOException, InterruptedException, RuntimeException {

        HttpResponse<String> response = null;
        do{
            rateLimitModule.checkAssociatedRateLimitBucketAndWait(request);
            rateLimitModule.checkGlobalRateLimitAndWait();
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
            rateLimitModule.associateRateLimitBucket(response);
            System.out.println(response.uri());
            System.out.println(response.statusCode());
            System.out.println(response.headers());
            System.out.println(response.body());
        }while(response.statusCode() == HttpStatusCode.TOO_MANY_REQUESTS.code);

        if(!HttpStatusCode.isStatusCodeAnSuccessCode(response.statusCode())){
            throw new HttpErrorStatusCodeException(fromJson(response.body(), JsonErrorDto.class));
        }
        return response;
    }

    private HttpRequest buildPut(String uri, Map<String, String> headers, Object data) throws URISyntaxException {
        return HttpRequest.newBuilder().uri(new URI(uri))
                .headers(headersToArray(headers))
                .PUT(HttpRequest.BodyPublishers.ofString(toJson(data)))
                .build();
    }

    private HttpRequest buildGet(String uri, Map<String, String> headers) throws URISyntaxException {
        return HttpRequest.newBuilder().uri(new URI(uri))
                .headers(headersToArray(headers))
                .GET()
                .build();
    }

    private HttpRequest buildPost(String uri, Map<String, String> headers, Object data) throws URISyntaxException {
        return HttpRequest.newBuilder().uri(new URI(uri))
                .headers(headersToArray(headers))
                .POST(HttpRequest.BodyPublishers.ofString(toJson(data)))
                .build();
    }

    private String[] headersToArray(Map<String, String> headers) {
        return headers.entrySet()
               .stream()
               .flatMap(it -> Stream.of(it.getKey(), it.getValue()))
               .toArray(String[]::new);
    }
}