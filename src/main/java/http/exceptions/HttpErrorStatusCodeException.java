package http.exceptions;

import dto.JsonErrorDto;

public class HttpErrorStatusCodeException extends RuntimeException{
    private JsonErrorDto jsonError;

    public JsonErrorDto getJsonError() {
        return jsonError;
    }

    public HttpErrorStatusCodeException(JsonErrorDto jsonError) {
        super(jsonError.getMessage());
        this.jsonError = jsonError;
    }
}
