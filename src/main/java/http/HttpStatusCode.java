package http;

public enum HttpStatusCode {
    OK(200),
    CREATED(201),
    NO_CONTENT(204),
    NOT_MODIFIED(304),
    BAD_REQUEST(400),
    UNAUTHORIZED(401),
    FORBIDDEN(403),
    NOT_FOUND(404),
    METHOD_NOT_ALLOWED(405),
    TOO_MANY_REQUESTS(429),
    GATEWAY_UNAVAILABLE(502),
    SERVER_ERROR(500);

    int code;
    HttpStatusCode(int code){ this.code = code; }

    public static boolean isStatusCodeAnSuccessCode(int statusCode){
        return statusCode/100 == 2;
    }

    }
