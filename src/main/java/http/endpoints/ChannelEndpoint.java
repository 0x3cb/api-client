package http.endpoints;

import dto.messages.BaseMessage;
import dto.messages.MessageDto;
import http.HttpRequestsSenderFactory;

import java.net.http.HttpResponse;

import static constants.Constants.BASE_URL;
import static utils.JsonUtils.fromHttpResponseToObject;

public class ChannelEndpoint {
    private final HttpRequestsSenderFactory requestsSender = HttpRequestsSenderFactory.getInstance();
    private static ChannelEndpoint instance;

    public static ChannelEndpoint getInstance() {
        if (instance == null)
            instance = new ChannelEndpoint();
        return instance;
    }

    private static final String GET_MESSAGE_URL_TEMPLATE = BASE_URL + "/channels/%s/messages/%s";

    private static final String SEND_MESSAGE_URL_TEMPLATE = BASE_URL + "/channels/%s/messages";

    public BaseMessage createMessage(String channelId, MessageDto message) {
        HttpResponse<String> response = requestsSender.doPost(SEND_MESSAGE_URL_TEMPLATE.formatted(channelId), message);
        return fromHttpResponseToObject(response, BaseMessage.class);
    }

    public BaseMessage getMessage(String channelId, String messageId){
        HttpResponse<String> response = requestsSender.doGet(GET_MESSAGE_URL_TEMPLATE.formatted(channelId, messageId));
        return fromHttpResponseToObject(response, BaseMessage.class);
    }

    private static final String CREATE_REACTION_URL_TEMPLATE = BASE_URL + "/channels/%s/messages/%s/reactions/%s/@me";

    public void createReaction(String channelId, String messageId, String emoji){
        requestsSender.doPut(CREATE_REACTION_URL_TEMPLATE.formatted(channelId, messageId, emoji), "");
    }
}
