package http.endpoints;

import http.HttpRequestsSenderFactory;

import java.net.http.HttpResponse;

import static constants.Constants.BASE_URL;
import static utils.JsonUtils.fromHttpResponseToObject;

public class UserEndpoint {
    private final HttpRequestsSenderFactory requestsSender = HttpRequestsSenderFactory.getInstance();
    private static UserEndpoint instance;

    public static UserEndpoint getInstance() {
        if (instance == null)
            instance = new UserEndpoint();
        return instance;
    }

    private static final String GET_USER_URL_TEMPLATE = BASE_URL + "/users/%s";

    public dto.user.User getUser(String userId){
        HttpResponse<String> response = requestsSender.doGet(GET_USER_URL_TEMPLATE.formatted(userId));
        return fromHttpResponseToObject(response, dto.user.User.class);
    }
}
