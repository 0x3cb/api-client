package http;

import java.net.URI;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class RateLimitModule {

    //These hashmaps provides association one to many (one endpoint to many rate limit Buckets)
    private final HashMap<String, String> endpointToRateLimitBucketId = new HashMap<>();
    private final HashMap<String, RateLimitBucket> rateLimitBucketIdToRateLimitBucket = new HashMap<>();

    private RateLimitBucket globalRateLimit;

    public RateLimitBucket checkAssociatedRateLimitBucketAndWait(HttpRequest request) throws InterruptedException {
        String rateLimitBucketId = endpointToRateLimitBucketId.get( String.format("%s %s", request.method(), removeIdsAndEmojisInUri(request.uri())) );
        RateLimitBucket rateLimitBucket = rateLimitBucketIdToRateLimitBucket.get(rateLimitBucketId);
        if( rateLimitBucket != null && rateLimitBucket.remaining == 0 ) {
            long resetTime = rateLimitBucket.reset - Instant.now().toEpochMilli();
            if( resetTime > 0) wait(resetTime);
        }
        return rateLimitBucket;
    }

    synchronized public void associateRateLimitBucket(HttpResponse<String> response){
        RateLimitBucket rateLimitBucket = parseRateLimitBucketFromHeaders(response.headers());
        if (rateLimitBucket.isGlobal()){
            globalRateLimit = rateLimitBucket;
        }else{
            endpointToRateLimitBucketId.put(String.format("%s %s", response.request().method(), removeIdsAndEmojisInUri(response.uri())), rateLimitBucket.id);
            rateLimitBucketIdToRateLimitBucket.put(rateLimitBucket.id, rateLimitBucket);
        }
    }

    public void checkGlobalRateLimitAndWait() throws InterruptedException {
        if(globalRateLimit != null){
            long resetTime = globalRateLimit.reset - Instant.now().toEpochMilli();
            if( resetTime > 0) wait(resetTime);
        }
    }

    private RateLimitBucket parseRateLimitBucketFromHeaders(HttpHeaders headers){
        Map<String, List<String>> headersMap = headers.map();

        String id = null;
        int limit, remaining, resetAfter = remaining = limit = 0;
        long reset = 0;
        Boolean global = false;

        if (headersMap.containsKey("X-RateLimit-Global")){
            global = true;
            reset = Instant.now().toEpochMilli() + Integer.parseInt(headersMap.get("Retry-After").get(0))*1000;
        }else{
            headersMap.get("X-RateLimit-Bucket").get(0);
            limit = Integer.parseInt(headersMap.get("X-RateLimit-Limit").get(0));
            remaining = Integer.parseInt(headersMap.get("X-RateLimit-Remaining").get(0));
            reset = Long.parseLong(headersMap.get("X-RateLimit-Reset").get(0));
            resetAfter = Integer.parseInt(headersMap.get("X-RateLimit-Reset-After").get(0));
        }

        return new RateLimitBucket(id, limit, remaining, reset, resetAfter, global);
    }

    private String removeIdsAndEmojisInUri(URI uri){
        return uri.getPath().replaceAll("/(\\d|[\\ud83c\\udf00-\\ud83d\\ude4f]|[\\ud83d\\ude80-\\ud83d\\udeff])+/", "/");
    }

    static class RateLimitBucket {
        String id;
        int limit;
        int remaining;
        long reset;
        int resetAfter;;
        Boolean global;

        public boolean isGlobal(){ return global; }

        public RateLimitBucket(String id, int limit, int remaining, long reset, int resetAfter, Boolean global) {
            this.id = id;
            this.limit = limit;
            this.remaining = remaining;
            this.reset = reset;
            this.resetAfter = resetAfter;
            this.global = global;
        }
    }

}
