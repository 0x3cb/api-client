package utils;

import com.google.gson.Gson;
import dto.Payload;

import java.lang.reflect.Type;
import java.net.http.HttpResponse;

public class JsonUtils {
    public static final Gson gson = new Gson();

    public static String toJson(Object obj) { return gson.toJson(obj); }

    public static <T> T fromJson(String json, Class<T> clazz) {
        return gson.fromJson(json, clazz);
    }

    public static <T> Payload<T> payloadFromJson(String json, Type typeOfT) {
        return gson.fromJson(json, typeOfT);
    }

    public static <T> T fromHttpResponseToObject(HttpResponse<String> response, Class<T> clazz) {
        String body = response.body();
        return JsonUtils.fromJson(body, clazz);
    }
}