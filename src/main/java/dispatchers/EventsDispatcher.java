package dispatchers;

import com.neovisionaries.ws.client.WebSocket;
import dto.Payload;
import dto.ReadyPayloadData;
import dto.messages.MessageDto;
import dto.messages.events.MessageDeleteEventDto;
import dto.messages.events.MessageTypingEventDto;
import http.endpoints.ChannelEndpoint;

import static state.SessionState.session;

public class EventsDispatcher {

    private final ChannelEndpoint channelEndpoint = ChannelEndpoint.getInstance();
    private final CommandsDispatcher commandsDispatcher = new CommandsDispatcher();

    public void handle(WebSocket socket, Payload payload) {
        session().setSequence(payload.getSequence());
        switch (payload.getEventName()) {
            case READY -> onReady(socket, payload);
            case MESSAGE_CREATE -> onMessageCreate(socket, payload);
            case TYPING_START -> onTyping(socket, payload);
            case MESSAGE_DELETE -> onMessageDelete(socket, payload);
            case MESSAGE_UPDATE -> onMessageUpdate(socket, payload);
        }
    }

    private void onMessageUpdate(WebSocket socket, Payload<MessageDto> payload) {
        MessageDto message = payload.getData();
        channelEndpoint.createReaction(message.getChannelId(), message.getId(), "\uD83D\uDC40");
    }

    private void onMessageCreate(WebSocket socket, Payload<MessageDto> payload) {
        commandsDispatcher.handle(payload.getData());
    }

    private void onReady(WebSocket websocket, Payload<ReadyPayloadData> payload) {
        session().setSessionId(payload.getData().getSessionId());
    }

    private void onTyping(WebSocket websocket, Payload<MessageTypingEventDto> payload) {
    }

    private void onMessageDelete(WebSocket websocket, Payload<MessageDeleteEventDto> payload){
    }
}