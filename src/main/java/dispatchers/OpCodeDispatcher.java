package dispatchers;

import com.neovisionaries.ws.client.WebSocket;
import dto.HelloPayloadData;
import dto.IdentifyPayloadData;
import dto.Payload;
import dto.ResumePayloadData;
import factories.HeartbeatSenderFactory;

import static com.neovisionaries.ws.client.WebSocketCloseCode.NONE;
import static constants.Constants.*;
import static dto.enums.GatewayOpcode.IDENTIFY;
import static dto.enums.GatewayOpcode.RESUME;
import static state.SessionState.session;
import static utils.JsonUtils.toJson;

public class OpCodeDispatcher {

    public void handle(WebSocket socket, Payload payload) {
        switch (payload.getOpcode()) {
            case RECONNECT -> onReconnect(socket, payload);
            case INVALID_SESSION -> onInvalidSession(socket, payload);
            case HELLO -> onHello(socket, payload);
            case HEARTBEAT_ACK -> onHeartBeatACK(socket, payload);
        }
    }

    private void onHeartBeatACK(WebSocket socket, Payload payload) {
        session().setAckBetweenHeartBeats(true);
    }

    private void onReconnect(WebSocket socket, Payload payload) {
        HeartbeatSenderFactory.getInstance().getHeartbeatSender().setInterval(0);
        session().setRenewable(true);
        socket.disconnect(NONE);
    }

    private void onInvalidSession(WebSocket socket, Payload payload) {
        HeartbeatSenderFactory.getInstance().getHeartbeatSender().setInterval(0);
        session().setRenewable(payload.getData().equals(true));
        socket.disconnect(NONE);
    }

    private void onHello(WebSocket socket, Payload<HelloPayloadData> payload) {
        HelloPayloadData helloPayloadData = payload.getData();
        HeartbeatSenderFactory.getInstance().getHeartbeatSender().setInterval(helloPayloadData.getHeartbeatInterval());
        if (session().isRenewable()) {
            resume(socket);
        } else {
            sendIdentify(socket);
        }
    }

    private void resume(WebSocket socket) {
        String JSONResumePayload = toJson(resumePayload());
        socket.sendText(JSONResumePayload);
        System.out.printf("> [%s] %s\n", RESUME, JSONResumePayload);
        session().setRenewable(false);
    }

    private Payload<ResumePayloadData> resumePayload() {
        ResumePayloadData payloadData = new ResumePayloadData(
                TOKEN,
                session().getSessionId(),
                session().getSequence()
        );

        return new Payload<>(RESUME, payloadData);
    }

    private void sendIdentify(WebSocket socket) {
        Payload<IdentifyPayloadData> identifyPayload = identifyPayload();
        String JSONIdentifyPayload = toJson(identifyPayload);
        socket.sendText(JSONIdentifyPayload);
        System.out.printf("> [%s] %s\n", IDENTIFY, toJson(identifyPayload.getData()));
    }

    private Payload<IdentifyPayloadData> identifyPayload() {
        IdentifyPayloadData payloadData = new IdentifyPayloadData(
                TOKEN,
                INTENTS,
                OS,
                BROWSER,
                DEVICE
        );

        return new Payload<IdentifyPayloadData>(IDENTIFY, payloadData);
    }
}
