package dispatchers;

import commandModules.VoteBan;
import dto.messages.BaseMessage;
import dto.messages.MessageDto;
import http.endpoints.ChannelEndpoint;

import java.util.Arrays;

public class CommandsDispatcher {

    private final ChannelEndpoint channelEndpoint = ChannelEndpoint.getInstance();

    public void handle( MessageDto message){
        try{
            String[][] commandAndArgs = parseCommandAndArgs(message.getContent());
            if (commandAndArgs == null) return;
            switch (commandAndArgs[0][0]){
                case ".voteBan" -> handleVoteBan(message, commandAndArgs[1]);
            }
        }catch (IllegalArgumentException exception){
            MessageDto newMessage = new BaseMessage(exception.getMessage());
            channelEndpoint.createMessage(message.getChannelId(), newMessage);
        }
    }

    private void handleVoteBan(MessageDto message, String[] args) throws IllegalArgumentException {
        System.out.println(message.getContent());
        if(message.getMentions().length == 0 )
            throw new IllegalArgumentException("User mention required!");
        new VoteBan(message.getChannelId(), message.getMentions()[0], 60000).startVote();

    }

    private String[][] parseCommandAndArgs(String message){
        if (!message. matches("^\\.\\w+\\s.*")) return null;
        String[] commandAndArgs = message.split(" ");
        String[] args = commandAndArgs.length > 1 ? Arrays.copyOfRange(commandAndArgs, 1, commandAndArgs.length - 1) : null;
        String[] command = {commandAndArgs[0]};
        return new String[][]{command, args};
    }
}
