package dto;

import com.google.gson.annotations.SerializedName;

public class ResumePayloadData {
    @SerializedName("token")
    private String token;

    @SerializedName("session_id")
    private String sessionId;

    @SerializedName("seq")
    private Integer sequence;

    public ResumePayloadData(String token, String sessionId, Integer sequence) {
        this.token = token;
        this.sessionId = sessionId;
        this.sequence = sequence;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }
}
