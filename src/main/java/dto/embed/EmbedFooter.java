package dto.embed;

import com.google.gson.annotations.SerializedName;

public class EmbedFooter {
    private String text;
    @SerializedName("icon_url")
    private String iconUrl;
    @SerializedName("proxy_icon_url")
    private String proxy_icon_url;

    public EmbedFooter(String text, String iconUrl, String proxy_icon_url) {
        this.text = text;
        this.iconUrl = iconUrl;
        this.proxy_icon_url = proxy_icon_url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getProxy_icon_url() {
        return proxy_icon_url;
    }

    public void setProxy_icon_url(String proxy_icon_url) {
        this.proxy_icon_url = proxy_icon_url;
    }
}
