package dto.embed;

import com.google.gson.annotations.SerializedName;

public class EmbedImage {
    private String url;
    @SerializedName("proxy_url")
    private String proxyUrl;
    private Integer height;
    private Integer width;

    public EmbedImage(String url, String proxyUrl, Integer height, Integer width) {
        this.url = url;
        this.proxyUrl = proxyUrl;
        this.height = height;
        this.width = width;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getProxyUrl() {
        return proxyUrl;
    }

    public void setProxyUrl(String proxyUrl) {
        this.proxyUrl = proxyUrl;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }
}
