package dto.embed;

import com.google.gson.annotations.SerializedName;

public class EmbedThumbnail {
    private String url;
    @SerializedName("proxy_url")
    private String proxyUrl;
    private Integer height;
    private Integer width;


}
