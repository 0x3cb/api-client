package dto;

import com.google.gson.annotations.SerializedName;

public class HelloPayloadData {
    @SerializedName("heartbeat_interval")
    private long heartbeatInterval;

    public long getHeartbeatInterval() {
        return heartbeatInterval;
    }

    public void setHeartbeatInterval(long heartbeatInterval) {
        this.heartbeatInterval = heartbeatInterval;
    }

    @Override
    public String toString() {
        return "HelloPayloadData{" +
                "heartbeatInterval=" + heartbeatInterval +
                '}';
    }
}
