package dto;

import com.google.gson.*;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import dto.enums.GatewayEvent;
import dto.enums.GatewayOpcode;
import dto.messages.BaseMessage;
import dto.messages.events.BaseMessageEvent;

import java.lang.reflect.Type;

@JsonAdapter(Payload.Serializer.class)
public class Payload<T> {

    @SerializedName("op")
    private GatewayOpcode opcode;

    @SerializedName("d")
    private T data;

    @SerializedName("s")
    private Integer sequence;

    @SerializedName("t")
    private GatewayEvent eventName;

    public Payload(GatewayOpcode opcode, T data) {
        this.opcode = opcode;
        this.data = data;
        this.sequence = null;
        this.eventName = null;
    }

    public Payload(int opCode, T data, Integer sequence, GatewayEvent eventName) {
        this.opcode = GatewayOpcode.getOpcodeByCode(opCode);
        this.data = data;
        this.sequence = sequence;
        this.eventName = eventName;
    }

    public Payload(GatewayOpcode opCode, T data, Integer sequence, GatewayEvent eventName) {
        this.opcode = opCode;
        this.data = data;
        this.sequence = sequence;
        this.eventName = eventName;
    }

    public GatewayOpcode getOpcode() {
        return opcode;
    }

    public void setOpcode(GatewayOpcode opcode) {
        this.opcode = opcode;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public GatewayEvent getEventName() {
        return eventName;
    }

    public void setEventName(GatewayEvent eventName) {
        this.eventName = eventName;
    }

    static class Serializer implements JsonDeserializer<Payload<?>> {
        @Override
        public Payload<?> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
            JsonObject jsonObject = json.getAsJsonObject();
            GatewayEvent event = context.deserialize(jsonObject.get("t"), GatewayEvent.class);
            GatewayOpcode opcode = context.deserialize(jsonObject.get("op"), GatewayOpcode.class);
            Integer sequence = jsonObject.get("s") != JsonNull.INSTANCE ? jsonObject.get("s").getAsInt(): null;
            Type typeOfData = computeTypeOfData(opcode, event);
            return new Payload<>(opcode, context.deserialize(jsonObject.get("d"), typeOfData), sequence, event);
        }
    }

    static private Type computeTypeOfData(GatewayOpcode opcode, GatewayEvent event){
        return opcode == GatewayOpcode.DISPATCH
                ? resolveTypeByEvent(event)
                : resolveTypeByOpcode(opcode);
    }

    private static Type resolveTypeByOpcode(GatewayOpcode opcode) {
        return switch (opcode) {
            case HELLO -> new TypeToken<HelloPayloadData>() {}.getType();
            default -> new TypeToken<LinkedTreeMap<?, ?>>() {}.getType();
        };
    }

    private static Type resolveTypeByEvent(GatewayEvent event) {
        return switch (event) {
            case READY -> new TypeToken<ReadyPayloadData>() {}.getType();
            case MESSAGE_CREATE, MESSAGE_UPDATE -> new TypeToken<BaseMessage>() {}.getType();
            case TYPING_START, MESSAGE_DELETE -> new TypeToken<BaseMessageEvent>() {}.getType();
            default -> new TypeToken<LinkedTreeMap<?, ?>>() {}.getType();
        };
    }
}
