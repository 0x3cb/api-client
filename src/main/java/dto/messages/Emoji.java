package dto.messages;

public class Emoji {
    private String id;
    private String name;
    //private Role[] roles;
    //private User user;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
