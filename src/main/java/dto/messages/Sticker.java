package dto.messages;

import com.google.gson.annotations.SerializedName;

public class Sticker {
    private String id;
    private String pack_id;
    private String name;
    private String description;
    private String tags;
    private String asset;
    @SerializedName("preview_asset")
    private String previewAsset;
    @SerializedName("format_type")
    private Integer formatType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPack_id() {
        return pack_id;
    }

    public void setPack_id(String pack_id) {
        this.pack_id = pack_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public String getPreviewAsset() {
        return previewAsset;
    }

    public void setPreviewAsset(String previewAsset) {
        this.previewAsset = previewAsset;
    }

    public Integer getFormatType() {
        return formatType;
    }

    public void setFormatType(Integer formatType) {
        this.formatType = formatType;
    }
}
