package dto.messages;

import com.google.gson.annotations.SerializedName;

public class MessageReference {
    @SerializedName("message_id")
    private String messageId;
    @SerializedName("channel_id")
    private String channelId;
    @SerializedName("guild_id")
    private String guildId;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getGuildId() {
        return guildId;
    }

    public void setGuildId(String guildId) {
        this.guildId = guildId;
    }
}
