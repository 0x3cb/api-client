package dto.messages;

import com.google.gson.annotations.SerializedName;

public class MessageActivity {
    private Integer type;
    @SerializedName("party_id")
    private String partyId;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }
}
