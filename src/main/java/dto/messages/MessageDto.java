package dto.messages;

import dto.embed.Embed;
import dto.enums.MessageType;
import dto.user.GuildMember;
import dto.user.Role;
import dto.user.User;

public interface MessageDto {

    public String getId();

    public void setId(String id);

    public User getAuthor();

    public void setAuthor(User author);

    public GuildMember getMember();

    public void setMember(GuildMember member);

    public String getContent();

    public String getChannelId();

    public void setChannelId(String channelId);

    public String getGuildId();

    public void setGuildId(String guildId);

    public void setContent(String content);

    public String getTimestamp();

    public void setTimestamp(String timestamp);

    public String getEditedTimestamp();

    public void setEditedTimestamp(String editedTimestamp);

    public boolean isTts();

    public void setTts(boolean tts);

    public boolean isMentionEveryone();

    public void setMentionEveryone(boolean mentionEveryone);

    public User[] getMentions();

    public void setMentions(User[] mentions);

    public Role[] getMentionRoles();

    public void setMentionRoles(Role[] mentionRoles);

    public ChannelMention[] getMentionChannels();

    public void setMentionChannels(ChannelMention[] mentionChannels);

    public Attachment[] getAttachments();

    public void setAttachments(Attachment[] attachments);

    public Embed[] getEmbeds();

    public void setEmbeds(Embed[] embeds);

    public Reaction[] getReactions();

    public void setReactions(Reaction[] reactions);

    public String getNonce();

    public void setNonce(String nonce);

    public boolean isPinned();

    public void setPinned(boolean pinned);

    public String getWebhookId();

    public void setWebhookId(String webhookId);

    public MessageType getType();

    public void setType(MessageType type);

    public MessageActivity getActivity();

    public void setActivity(MessageActivity activity);

    public MessageApplication getApplication();

    public void setApplication(MessageApplication application);

    public MessageReference getMessageReference();

    public void setMessageReference(MessageReference messageReference);

    public Integer getFlags();

    public void setFlags(Integer flags);

    public Sticker getStickers();

    public void setStickers(Sticker stickers);

    public MessageDto getReferencedMessage();

    public void setReferencedMessage(MessageDto referencedMessage);
}
