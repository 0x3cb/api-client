package dto.messages;

import com.google.gson.annotations.SerializedName;
import dto.embed.Embed;
import dto.enums.MessageType;
import dto.user.GuildMember;
import dto.user.Role;
import dto.user.User;

public class BaseMessage implements MessageDto {
    private String id;
    private User author;
    private GuildMember member;

    @SerializedName("channel_id")
    private String channelId;
    @SerializedName("guild_id")
    private String guildId;

    private String content;
    private String timestamp;
    @SerializedName("edited_timestamp")
    private String editedTimestamp;
    private boolean tts;
    @SerializedName("mention_everyone")
    private boolean mentionEveryone;
    private User[] mentions;
    @SerializedName("mention_roles")
    private Role[] mentionRoles;
    @SerializedName("mention_channels")
    private ChannelMention[] mentionChannels;

    private Attachment[] attachments;
    private Embed[] embeds;
    private Reaction[] reactions;
    private String nonce;
    private boolean pinned;

    @SerializedName("webhook_id")
    private String webhookId;
    private MessageType type;
    private MessageActivity activity;
    private MessageApplication application;
    @SerializedName("message_reference")
    private MessageReference messageReference;
    private Integer flags;
    private Sticker stickers;

    public BaseMessage() { }

    public BaseMessage(String content) {
        this.content = content;
    }

    @SerializedName("referenced_message")
    private MessageDto referencedMessage;
    
    @Override
	public String getId() {
        return id;
    }

    @Override
	public void setId(String id) {
        this.id = id;
    }

    @Override
	public User getAuthor() {
        return author;
    }

    @Override
	public void setAuthor(User author) {
        this.author = author;
    }

    @Override
	public GuildMember getMember() {
        return member;
    }

    @Override
	public void setMember(GuildMember member) {
        this.member = member;
    }

    @Override
	public String getContent() {
        return content;
    }

    @Override
	public void setContent(String content) {
        this.content = content;
    }

    @Override
	public String getTimestamp() {
        return timestamp;
    }

    @Override
	public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
	public String getEditedTimestamp() {
        return editedTimestamp;
    }

    @Override
	public void setEditedTimestamp(String editedTimestamp) {
        this.editedTimestamp = editedTimestamp;
    }

    @Override
	public boolean isTts() {
        return tts;
    }

    @Override
	public void setTts(boolean tts) {
        this.tts = tts;
    }

    @Override
	public boolean isMentionEveryone() {
        return mentionEveryone;
    }

    @Override
	public void setMentionEveryone(boolean mentionEveryone) {
        this.mentionEveryone = mentionEveryone;
    }

    @Override
	public User[] getMentions() {
        return mentions;
    }

    @Override
	public void setMentions(User[] mentions) {
        this.mentions = mentions;
    }

    @Override
	public Role[] getMentionRoles() {
        return mentionRoles;
    }

    @Override
	public void setMentionRoles(Role[] mentionRoles) {
        this.mentionRoles = mentionRoles;
    }

    @Override
	public ChannelMention[] getMentionChannels() {
        return mentionChannels;
    }

    @Override
	public void setMentionChannels(ChannelMention[] mentionChannels) {
        this.mentionChannels = mentionChannels;
    }

    @Override
	public Attachment[] getAttachments() {
        return attachments;
    }

    @Override
	public void setAttachments(Attachment[] attachments) {
        this.attachments = attachments;
    }

    @Override
	public Embed[] getEmbeds() {
        return embeds;
    }

    @Override
	public void setEmbeds(Embed[] embeds) {
        this.embeds = embeds;
    }

    @Override
	public Reaction[] getReactions() {
        return reactions;
    }

    @Override
	public void setReactions(Reaction[] reactions) {
        this.reactions = reactions;
    }

    @Override
	public String getNonce() {
        return nonce;
    }

    @Override
	public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    @Override
	public boolean isPinned() {
        return pinned;
    }

    @Override
	public void setPinned(boolean pinned) {
        this.pinned = pinned;
    }

    @Override
	public String getWebhookId() {
        return webhookId;
    }

    @Override
	public void setWebhookId(String webhookId) {
        this.webhookId = webhookId;
    }

    @Override
	public MessageType getType() {
        return type;
    }

    @Override
	public void setType(MessageType type) {
        this.type = type;
    }

    @Override
	public MessageActivity getActivity() {
        return activity;
    }

    @Override
	public void setActivity(MessageActivity activity) {
        this.activity = activity;
    }

    @Override
	public MessageApplication getApplication() {
        return application;
    }

    @Override
	public void setApplication(MessageApplication application) {
        this.application = application;
    }

    @Override
	public MessageReference getMessageReference() {
        return messageReference;
    }

    @Override
	public void setMessageReference(MessageReference messageReference) {
        this.messageReference = messageReference;
    }

    @Override
	public Integer getFlags() {
        return flags;
    }

    @Override
	public void setFlags(Integer flags) {
        this.flags = flags;
    }

    @Override
	public Sticker getStickers() {
        return stickers;
    }

    @Override
	public void setStickers(Sticker stickers) {
        this.stickers = stickers;
    }

    @Override
	public MessageDto getReferencedMessage() {
        return referencedMessage;
    }

    @Override
	public void setReferencedMessage(MessageDto referencedMessage) {
        this.referencedMessage = referencedMessage;
    }

    @Override
    public String getGuildId() {
        return guildId;
    }

    @Override
    public void setGuildId(String guildId) {
        this.guildId = guildId;
    }

    @Override
    public String getChannelId() {
        return channelId;
    }

    @Override
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
}
