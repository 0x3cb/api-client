package dto.messages.events;

import com.google.gson.annotations.SerializedName;
import dto.user.GuildMember;

public class BaseMessageEvent implements MessageDeleteEventDto, MessageDeleteBulkEventDto, MessageTypingEventDto {

    private String id;

    private String[] ids;

    @SerializedName("channel_id")
    private String channelId;

    @SerializedName("guild_id")
    private String guildId;

    @SerializedName("user_id")
    private String userId;

    private String timestamp;

    private GuildMember member;

    @Override
    public String[] getIds() {
        return ids;
    }

    @Override
    public void setIds(String[] ids) {
        this.ids = ids;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getChannelId() {
        return channelId;
    }

    @Override
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    @Override
    public String getGuildId() {
        return guildId;
    }

    @Override
    public void setGuildId(String guildId) {
        this.guildId = guildId;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String getTimestamp() {
        return timestamp;
    }

    @Override
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public GuildMember getMember() {
        return member;
    }

    @Override
    public void setMember(GuildMember member) {
        this.member = member;
    }
}
