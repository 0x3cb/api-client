package dto.messages.events;

import dto.user.GuildMember;

public interface MessageTypingEventDto {

    public String getChannelId();

    public void setChannelId(String channelId);

    public String getGuildId();

    public void setGuildId(String guildId);

    public String getUserId();

    public void setUserId(String userId);

    public String getTimestamp();

    public void setTimestamp(String timestamp);

    public GuildMember getMember();

    public void setMember(GuildMember member);
}
