package dto.messages.events;

public interface MessageDeleteEventDto {

    public String getId();

    public void setId(String id);

    public String getChannelId();

    public void setChannelId(String channelId);

    public String getGuildId();

    public void setGuildId(String guildId);
}
