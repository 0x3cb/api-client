package dto.messages.events;

import com.google.gson.annotations.SerializedName;

public interface MessageDeleteBulkEventDto {

    public String[] getIds();

    public void setIds(String[] ids);

    public String getChannelId();

    public void setChannelId(String channelId);

    public String getGuildId();

    public void setGuildId(String guildId);
}
