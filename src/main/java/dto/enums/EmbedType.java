package dto.enums;

public enum EmbedType {
    // Generic embed rendered from embed attributes
    RICH,
    // Image embed
    IMAGE,
    // Video embed
    VIDEO,
    // Animated gif image embed rendered as a video embed
    GIFV,
    // Article embed
    ARTICLE,
    //  Link embed
    LINK;
}
