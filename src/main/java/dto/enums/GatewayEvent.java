package dto.enums;

public enum GatewayEvent {
    //  Defines the heartbeat interval
    HELLO,

    //  Contains the initial state information
    READY,

    //  Response to Resume
    RESUMED,

    //  Server is going away, client should reconnect to gateway and resume
    RECONNECT,

    //  Failure response to Identify or Resume or invalid active session
    INVALID_SESSION,

    //  New guild channel created
    CHANNEL_CREATE,

    //  Channel was updated
    CHANNEL_UPDATE,

    //  Channel was deleted
    CHANNEL_DELETE,

    //  Message was pinned or unpinned
    CHANNEL_PINS_UPDATE,

    //  Lazy-load for unavailable guild, guild became available, or user joined a new guild
    GUILD_CREATE,

    //  Guild was updated
    GUILD_UPDATE,

    //  Guild became unavailable, or user left/was removed from a guild
    GUILD_DELETE,

    //  User was banned from a guild
    GUILD_BAN_ADD,

    //	User was unbanned from a guild
    GUILD_BAN_REMOVE,

    //	Guild emojis were updated
    GUILD_EMOJIS_UPDATE,

    //	Guild integration was updated
    GUILD_INTEGRATIONS_UPDATE,

    //	New user joined a guild
    GUILD_MEMBER_ADD,

    //	User was removed from a guild
    GUILD_MEMBER_REMOVE,

    //	Guild member was updated
    GUILD_MEMBER_UPDATE,

    //	Response to Request Guild Members
    GUILD_MEMBERS_CHUNK,

    //	Guild role was created
    GUILD_ROLE_CREATE,

    //	Guild role was updated
    GUILD_ROLE_UPDATE,

    //	Guild role was deleted
    GUILD_ROLE_DELETE,

    //	Invite to a channel was created
    INVITE_CREATE,

    //	Invite to a channel was deleted
    INVITE_DELETE,

    //	Message was created
    MESSAGE_CREATE,

    //	Message was edited
    MESSAGE_UPDATE,

    //	Message was deleted
    MESSAGE_DELETE,

    //	Multiple messages were deleted at once
    MESSAGE_DELETE_BULK,

    //	User reacted to a message
    MESSAGE_REACTION_ADD,

    //	User removed a reaction from a message
    MESSAGE_REACTION_REMOVE,

    //	All reactions were explicitly removed from a message
    MESSAGE_REACTION_REMOVE_ALL,

    //	All reactions for a given emoji were explicitly removed from a message
    MESSAGE_REACTION_REMOVE_EMOJI,

    //  User was updated
    PRESENCE_UPDATE,

    //  User started typing in a channel
    TYPING_START,

    //  Properties about the user changed
    USER_UPDATE,

    //  Someone joined, left, or moved a voice channel
    VOICE_STATE_UPDATE,

    //  Guild's voice server was updated
    VOICE_SERVER_UPDATE,

    //  Guild channel webhook was created, update, or deleted
    WEBHOOKS_UPDATE	,

    //  User used a Slash Command
    INTERACTION_CREATE;
}
