package dto.enums;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.annotations.JsonAdapter;
import java.lang.reflect.Type;

@JsonAdapter(GatewayOpcode.Serializer.class)
public enum GatewayOpcode {
    DISPATCH(0),
    HEARTBEAT(1),
    IDENTIFY(2),
    PRESENCE_UPDATE(3),
    VOICE_STATE_UPDATE(4),
    RESUME(6),
    RECONNECT(7),
    REQUEST_GUILD_MEMBERS(8),
    INVALID_SESSION(9),
    HELLO(10),
    HEARTBEAT_ACK(11);

    public final int code;

    GatewayOpcode(int code){
        this.code = code;
    }

    public static GatewayOpcode getOpcodeByCode(int code) {
        for (GatewayOpcode gatewayOpcode : values())
            if (gatewayOpcode.code == code) return gatewayOpcode;
        return null;
    }

    static class Serializer implements JsonSerializer<GatewayOpcode>, JsonDeserializer<GatewayOpcode> {
        @Override
        public JsonElement serialize(GatewayOpcode src, Type typeOfSrc, JsonSerializationContext context) {
            return context.serialize(src.code);
        }

        @Override
        public GatewayOpcode deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
            try {
                return getOpcodeByCode(json.getAsNumber().intValue());
            } catch (JsonParseException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
