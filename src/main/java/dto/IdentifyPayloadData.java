package dto;

public class IdentifyPayloadData {

    private String token;

    private int intents;

    private IdentifyProperties properties;

    public IdentifyPayloadData(String token, int intents, String os, String browser, String device) {
        this.token = token;
        this.intents = intents;
        this.properties = new IdentifyProperties(os, browser, device);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getIntents() {
        return intents;
    }

    public void setIntents(int intents) {
        this.intents = intents;
    }

    public IdentifyProperties getProperties() {
        return properties;
    }

    public void setProperties(IdentifyProperties properties) {
        this.properties = properties;
    }

    private static class IdentifyProperties {
        String os;
        String browser;
        String device;

        private IdentifyProperties(String os, String browser, String device) {
            this.os = os;
            this.browser = browser;
            this.device = device;
        }
    }
}
