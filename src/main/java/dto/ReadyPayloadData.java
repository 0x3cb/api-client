package dto;

import com.google.gson.annotations.SerializedName;
import dto.user.User;

public class ReadyPayloadData {
    @SerializedName("v")
    private Integer gatewayVersion;
    private User user;
    @SerializedName("session_id")
    private String sessionId;

    public Integer getGatewayVersion() {
        return gatewayVersion;
    }

    public User getUser() {
        return user;
    }

    public String getSessionId() {
        return sessionId;
    }

    @Override
    public String toString() {
        return "ReadyPayloadData{" +
                "gatewayVersion=" + gatewayVersion +
                ", user=" + user +
                ", sessionId='" + sessionId + '\'' +
                '}';
    }
}
