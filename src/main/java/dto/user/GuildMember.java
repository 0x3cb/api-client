package dto.user;

import com.google.gson.annotations.SerializedName;

public class GuildMember {
    private User user;
    private String nick;
    private String[] roles;
    @SerializedName("joined_at")
    private String joinedAt;
    @SerializedName("premium_since")
    private String premiumSince;
    private boolean deaf;
    private boolean mute;
    private boolean pending;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String[] getRoles() {
        return roles;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }

    public String getJoinedAt() {
        return joinedAt;
    }

    public void setJoinedAt(String joinedAt) {
        this.joinedAt = joinedAt;
    }

    public String getPremiumSince() {
        return premiumSince;
    }

    public void setPremiumSince(String premiumSince) {
        this.premiumSince = premiumSince;
    }

    public boolean isDeaf() {
        return deaf;
    }

    public void setDeaf(boolean deaf) {
        this.deaf = deaf;
    }

    public boolean isMute() {
        return mute;
    }

    public void setMute(boolean mute) {
        this.mute = mute;
    }

    public boolean isPending() {
        return pending;
    }

    public void setPending(boolean pending) {
        this.pending = pending;
    }
}
