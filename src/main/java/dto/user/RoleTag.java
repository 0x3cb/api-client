package dto.user;

import com.google.gson.annotations.SerializedName;

public class RoleTag {
    @SerializedName("bot_id")
    private String botId;
    @SerializedName("integration_id")
    private String integrationId;
    @SerializedName("premium_subscriber")
    private boolean premiumSubscriber;

    public String getBotId() {
        return botId;
    }

    public void setBotId(String botId) {
        this.botId = botId;
    }

    public String getIntegrationId() {
        return integrationId;
    }

    public void setIntegrationId(String integrationId) {
        this.integrationId = integrationId;
    }

    public boolean isPremiumSubscriber() {
        return premiumSubscriber;
    }

    public void setPremiumSubscriber(boolean premiumSubscriber) {
        this.premiumSubscriber = premiumSubscriber;
    }
}
