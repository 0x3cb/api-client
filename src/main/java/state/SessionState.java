package state;

public class SessionState {
    private static SessionState instance;

    private String sessionId;

    private Integer sequence;

    private boolean renewable = false;

    private boolean ackBetweenHeartBeats = true;

    private SessionState() {}

    public static SessionState session() {
        if (instance == null) {
            instance = new SessionState();
        }
        return instance;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public boolean isRenewable() {
        return renewable;
    }

    public void setRenewable(boolean renewable) {
        this.renewable = renewable;
    }

    public boolean isAckBetweenHeartBeats() {
        return ackBetweenHeartBeats;
    }

    public void setAckBetweenHeartBeats(boolean ackBetweenHeartBeats) {
        this.ackBetweenHeartBeats = ackBetweenHeartBeats;
    }
}
