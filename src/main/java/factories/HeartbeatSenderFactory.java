package factories;

import com.neovisionaries.ws.client.WebSocket;
import dto.Payload;
import senders.HeartbeatSender;

import static dto.enums.GatewayOpcode.HEARTBEAT;
import static state.SessionState.session;
import static utils.JsonUtils.toJson;

public class HeartbeatSenderFactory {
    private static HeartbeatSenderFactory instance;
    private final HeartbeatSender heartbeatSender;

    private HeartbeatSenderFactory() {
        WebSocket webSocket = WebSocketFactory.getInstance().getWebSocket();
        heartbeatSender = new HeartbeatSender(webSocket, () -> {
            String heartBeatPayload = toJson(new Payload(HEARTBEAT, session().getSequence()));
            System.out.printf("> [%s] %s\n", HEARTBEAT.name(), session().getSequence());
            return heartBeatPayload.getBytes();
        });
    }

    public HeartbeatSender getHeartbeatSender() { return heartbeatSender; }

    public static HeartbeatSenderFactory getInstance() {
        if (instance == null)
            instance = new HeartbeatSenderFactory();
        return instance;
    }
}
