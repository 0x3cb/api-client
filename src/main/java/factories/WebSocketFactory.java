package factories;

import com.neovisionaries.ws.client.*;
import dispatchers.EventsDispatcher;
import dispatchers.OpCodeDispatcher;
import dto.Payload;
import utils.JsonUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static com.neovisionaries.ws.client.WebSocketState.CLOSED;
import static dto.enums.GatewayOpcode.DISPATCH;

public class WebSocketFactory {
    private static final String URI = "wss://gateway.discord.gg/?v=8";
    private static WebSocketFactory instance;

    private WebSocket webSocket;

    private WebSocketFactory() {
        try {
            webSocket = new com.neovisionaries.ws.client.WebSocketFactory().createSocket(URI);
            initListeners(webSocket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initListeners(WebSocket webSocket) {
        OpCodeDispatcher opCodeDispatcher = new OpCodeDispatcher();
        EventsDispatcher eventDispatcher = new EventsDispatcher();

        webSocket.addListener(new WebSocketAdapter() {
            @Override
            public void onFrame(WebSocket websocket, WebSocketFrame frame) {

                Payload payload = JsonUtils.payloadFromJson(frame.getPayloadText(), Payload.class);
                try{
                    System.out.printf("< [%s] %s\n", payload.getOpcode() == DISPATCH ? payload.getEventName() : payload.getOpcode(), payload.getData());
                    if (payload.getOpcode() == DISPATCH) {
                        eventDispatcher.handle(websocket, payload);
                    } else {
                        opCodeDispatcher.handle(websocket, payload);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onDisconnected(WebSocket websocket, WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame, boolean closedByServer) {
                System.err.printf("> Disconnected with code: %s Reason: %s Content: %s\n", serverCloseFrame.getCloseCode(), serverCloseFrame.getCloseReason(), serverCloseFrame.getPayloadText());
            }

            @Override
            public void onStateChanged(WebSocket websocket, WebSocketState newState) throws Exception {
                if (newState == CLOSED) {
                    setWebSocket(websocket.recreate().connect());
                }
            }

            @Override
            public void onError(WebSocket websocket, WebSocketException cause) {
                System.err.printf("> %s! cause: %s\n", "Error", cause);
            }

            @Override
            public void onFrameError(WebSocket websocket, WebSocketException cause, WebSocketFrame frame) {
                System.err.printf("> %s! cause: %s On frame: %s\n", "OnFrameError", cause, frame.getPayloadText());
            }

            @Override
            public void onConnected(WebSocket websocket, Map<String, List<String>> headers) {
                System.err.printf("> %s!\n", "Connected");
            }
        });
    }

    private void setWebSocket(WebSocket webSocket) { this.webSocket = webSocket; }

    public WebSocket getWebSocket() { return webSocket; }

    public static WebSocketFactory getInstance() {
        if (instance == null)
            instance = new WebSocketFactory();
        return instance;
    }
}
